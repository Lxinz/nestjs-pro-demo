import { ApiTags, ApiOperation } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { PostsService, PostsRo } from './posts.service';
import { CreatePostDto } from './dto/create-post.dot';
import { UpdatePostDto } from './dto/update-post.dot';
import { DeletePostDto } from './dto/delete-post.dot';

@ApiTags('文章')
@Controller('post')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  /**
   * 创建文章
   * @param post
   */
  @ApiOperation({ summary: '创建文章' })
  @Post()
  async create(@Body() post: CreatePostDto) {
    return await this.postsService.create(post);
  }

  /**
   * 获取所有文章
   */
  @ApiOperation({ summary: '获取所有文章' })
  @Get()
  findAll(@Query() query): Promise<PostsRo> {
    return this.postsService.findAll(query);
  }

  /**
   * 获取指定文章
   * @param id
   */
  @ApiOperation({ summary: '获取指定文章' })
  @Get(':id')
  async findById(@Param('id') id) {
    return await this.postsService.findById(id);
  }

  /**
   * 更新文章
   * @param post
   */
  @ApiOperation({ summary: '更新文章' })
  @Put()
  async update(@Body() post: UpdatePostDto) {
    return await this.postsService.updateById(post);
  }

  /**
   * 删除文章
   * @param id
   */
  @ApiOperation({ summary: '删除文章' })
  @Delete('deleteId')
  async remove(@Query('id') id: DeletePostDto) {
    return await this.postsService.remove(id);
  }
}
