import { ApiProperty } from '@nestjs/swagger';

import { IsNotEmpty, IsNumber } from 'class-validator';

export class UpdatePostDto {
  @ApiProperty({ description: '文章id' })
  @IsNotEmpty({ message: '缺少文章id' })
  @IsNumber()
  readonly id: number;

  @ApiProperty({ description: '文章标题' })
  @IsNotEmpty({ message: '缺少文章标题' })
  readonly title: string;

  @ApiProperty({ description: '文章作者' })
  @IsNotEmpty({ message: '缺少文章作者' })
  readonly author: string;

  @ApiProperty({ description: '文章内容' })
  readonly content: string;
}
