// 官方：数据传输对象（DTO)(Data Transfer Object)，是一种设计模式之间传输数据的软件应用系统。数据传输目标往往是数据访问对象从数据库中检索数据。数据传输对象与数据交互对象或数据访问对象之间的差异是一个以不具有任何行为除了存储和检索的数据（访问和存取器）。
// DTO 本身更像是一个指南, 在使用API时，方便我们了解请求期望的数据类型以及返回的数据对象。

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreatePostDto {
  @ApiProperty({ description: '文章标题' })
  @IsNotEmpty({ message: '缺少文章标题' })
  readonly title: string;

  @ApiProperty({ description: '文章作者' })
  @IsNotEmpty({ message: '缺少作者信息' })
  readonly author: string;

  @ApiProperty({ description: '文章内容' })
  readonly content: string;
}
