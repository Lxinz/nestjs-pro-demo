import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class DeletePostDto {
  @ApiProperty({ description: '文章id' })
  @IsNotEmpty({ message: '缺少文章id' })
  @IsNumber()
  readonly id: number;
}
