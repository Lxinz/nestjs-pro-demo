# nestjs-pro-demo

> **nestjs**
>
> Nest (NestJS) 是一个用于构建高效、可扩展的 `Node.js`[服务器](https://cloud.tencent.com/act/pro/promotion-cvm?from_column=20065&from=20065)端应用程序的开发框架。它利用 `JavaScript` 的渐进增强的能力，使用并完全支持 `TypeScript` （仍然允许开发者使用纯 JavaScript 进行开发），并结合了 OOP （面向对象编程）、FP （函数式编程）和 FRP （函数响应式编程）。

## 描述

nestjs项目模版，实现接口实现、对接mysql数据库（typeorm实现；Entity、Repository使用方式）、nestjs过滤器与拦截器实现和对接swagger接口文档（class-validator、dto字段校验、注释实现）。

## 安装

```bash
$ yarn install
```

## 运行应用

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## 接口实现

src/app.controller.ts；src/posts/posts.controller.ts。

## 数据库对接

src/app.module.ts；src/posts/posts.module.ts；src/posts/posts.entity.ts；src/posts/posts.service.ts。

## 过滤器、拦截器

src/core/filter；src/core/interceptor；main.ts。

## swagger接口文档

src/posts/posts.controller.ts；src/posts/dto/*.ts；main.ts。

## 项目部署

https://cloud.tencent.com/developer/article/1976319
