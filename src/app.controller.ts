import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';

@ApiTags('公共测试接口')
@Controller('api')
export class AppController {
  // 这里 AppService 不需要new  AppService(); 因为在app.module中providers中注册了
  constructor(private readonly appService: AppService) {}

  @Get('list')
  getList1(): string {
    return 'get methods';
  }

  // 通配符(?+* 三种通配符 )
  // 可以匹配到 get请求, http://localhost:3000/app/list_xxx
  @Get('list_*')
  getList2(): string {
    return 'get methods';
  }

  @Post('list')
  postList1(): string {
    return 'post methods';
  }

  @Put('list/:id')
  putList1(): string {
    return 'put methods';
  }
}
